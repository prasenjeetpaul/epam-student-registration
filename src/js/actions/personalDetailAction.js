import { PERSONAL_DETAIL_ACTION, APP_STATE_ACTION } from './actionType';
import { API } from "./../store/appState";

require ("babel-polyfill");

export const firstNameEntered = firstName => ({
    type: PERSONAL_DETAIL_ACTION.FIRST_NAME_ENTERED,
    payload: firstName
})

export const middleNameEntered = middleName => ({
    type: PERSONAL_DETAIL_ACTION.MIDDLE_NAME_ENTERED,
    payload: middleName
})

export const lastNameEntered = lastName => ({
    type: PERSONAL_DETAIL_ACTION.LAST_NAME_ENTERED,
    payload: lastName
})

export const dateOfBirthEntered = dateOfBirth => ({
    type: PERSONAL_DETAIL_ACTION.DATE_OF_BIRTH_ENTERED,
    payload: dateOfBirth
})

export const mobileNumberEntered = mobileNumber => ({
    type: PERSONAL_DETAIL_ACTION.MOBILE_NUMBER_ENTERED,
    payload: mobileNumber
})


export const personalDetailSubmitted = (history) => (dispatch, getState) => {
    let personalDetailAPI = API + "/user/secure/personalInfo";
    fetch(personalDetailAPI, {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            id: getState().appState.studentID,
            ...getState().personalDetail
        })
    }).then(response => response.text().then(data => {
        dispatch({
            type: APP_STATE_ACTION.PEROSNAL_DATA_FILLED,
            payload: true
        });
        history.push("/edit/address");
    }))
}