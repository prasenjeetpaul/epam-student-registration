import { REGISTER_ACTION, APP_STATE_ACTION } from "./actionType";
import  { API } from "./../store/appState";

require('es6-promise').polyfill();
require('isomorphic-fetch');

export const registerEmailEntered = emailId => ({
    type: REGISTER_ACTION.EMAIL_ENTERED,
    payload: emailId
})

export const registerPassword1Entered = password => ({
    type: REGISTER_ACTION.PASSWORD1_ENTERED,
    payload: password
})

export const registerPassword2Entered = password => ({
    type: REGISTER_ACTION.PASSWORD2_ENTERED,
    payload: password
})

export const registerClicked = () => (dispatch, getState) => {
    if (getState().register.password2 != getState().register.password1) {
        dispatch({
            type: APP_STATE_ACTION.REGISTER_FAILED,
            payload: "Password didn't matched"
        })
    } else {
        let verifyEmail = API + "/verifyEmail";
        fetch(verifyEmail, {
            // crossDomain: true,
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                emailId: getState().register.emailId
            })
            // mode: "no-cors"
        }).then(response => response.text()
            .then(data => {
                if(data == "true") {
                    let registerUser = API + "/registration";
                    fetch(registerUser, {
                        method: "POST",
                        headers: {
                            "Content-Type": "application/json"
                        },
                        body: JSON.stringify({
                            emailId: getState().register.emailId,
                            password: getState().register.password1
                        })
                    }).then(response => response.text()
                        .then(data => {
                            if (data == "false") {
                                dispatch({
                                    type: APP_STATE_ACTION.REGISTER_FAILED,
                                    payload: "Invalid inputs"
                                })
                            } else {
                                dispatch({
                                    type: APP_STATE_ACTION.REGISTER_SUCCESS,
                                    payload: data
                                })
                            }
                        }))
                } else {
                    dispatch({
                        type: APP_STATE_ACTION.REGISTER_FAILED,
                        payload: "Email ID already exists"
                    })
                }
            })
        )
    }
}