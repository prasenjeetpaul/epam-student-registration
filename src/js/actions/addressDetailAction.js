import { ADDRESS_DETAIL_ACTION, APP_STATE_ACTION } from './actionType';
import { API } from "./../store/appState";

export const typeEntered = type => ({
    type: ADDRESS_DETAIL_ACTION.TYPE_ENTERED,
    payload: type
})

export const houseNumberEntered = houseNumber => ({
    type: ADDRESS_DETAIL_ACTION.HOUSE_NUMBER_ENTERED,
    payload: houseNumber
})

export const streetEntered = street => ({
    type: ADDRESS_DETAIL_ACTION.STREET_ENTERED,
    payload: street
})

export const landmarkEntered = landmark => ({
    type: ADDRESS_DETAIL_ACTION.LANDMARK_ENTERED,
    payload: landmark
})

export const cityEntered = city => ({
    type: ADDRESS_DETAIL_ACTION.CITY_ENTERED,
    payload: city
})

export const stateEntered = state => ({
    type: ADDRESS_DETAIL_ACTION.STATE_ENTERED,
    payload: state
})

export const zipcodeEntered = zipcode => ({
    type: ADDRESS_DETAIL_ACTION.ZIPCODE_ENTERED,
    payload: zipcode
})

export const countryEntered = country => ({
    type: ADDRESS_DETAIL_ACTION.COUNTRY_ENTERED,
    payload: country
})

export const selectedCountryEntered = countryName => ({
    type: ADDRESS_DETAIL_ACTION.SELECTED_COUNTRY_ENTERED,
    payload: countryName
})

export const selectedStateEntered = stateName => ({
    type: ADDRESS_DETAIL_ACTION.SELECTED_STATE_ENTERED,
    payload: stateName
})

export const fetchCountry = () => (dispatch, getState) => {
    let countryAPI = API + "/address/getCountries";
    fetch(countryAPI)
        .then(resposne => resposne.text().then(data => {
            let countryArray = JSON.parse(data);
            dispatch(countryEntered(countryArray));
        }));
};

export const fetchState = countryName => (dispatch, getState) => {
    dispatch(selectedCountryEntered(countryName))
    let stateAPI = API + "/address/getStates?country=" + countryName;
    fetch(stateAPI)
        .then(resposne => resposne.text().then(data => {
            let stateArray = JSON.parse(data);
            dispatch(stateEntered(stateArray));
        }));
};

function cleanAddressData(addressData) {
    let cleanData = {
        ...addressData,
        state: addressData.selectedState,
        country: addressData.selectedYear
    }
    delete cleanData.selectedCountry;
    delete cleanData.selectedState;
    return cleanData;
}

export const addressDetailSubmitted = (history) => (dispatch, getState) => {
    let addressDetailAPI = API + "/user/secure/address";
    fetch(addressDetailAPI, {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            id: getState().appState.studentID,
            ...cleanAddressData(getState().addressDetail)
        })
    }).then(response => response.text().then(data => {
        dispatch({
            type: APP_STATE_ACTION.ADDRESS_DATA_FILLED,
            payload: true
        });
        history.push("/edit/academic");
    }))
    // fakeAPI().then(data => {
    //     console.log("Submitted Data");
    //     console.log(getState().addressDetail);
    //     if (data != null) {
    //         dispatch({
    //             type: APP_STATE_ACTION.ADDRESS_DATA_FILLED,
    //             payload: true
    //         });
    //         history.push("/edit/academic");
    //     }
    // });
}

export const AddressAction = {
    typeEntered: typeEntered,
    houseNumberEntered: houseNumberEntered,
    streetEntered: streetEntered,
    landmarkEntered: landmarkEntered,
    cityEntered: cityEntered,
    stateEntered: stateEntered,
    zipcodeEntered: zipcodeEntered,
    countryEntered: countryEntered,
    addressDetailSubmitted: addressDetailSubmitted,
    fetchCountry: fetchCountry
}