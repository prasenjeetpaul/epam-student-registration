import { ACADEMIC_DETAIL_ACTION, APP_STATE_ACTION} from "./actionType";
import { API } from "./../store/appState";

export const nameEntered = name => ({
    type: ACADEMIC_DETAIL_ACTION.NAME_ENTERED,
    payload: name
})

export const boardEntered = board => ({
    type: ACADEMIC_DETAIL_ACTION.BOARD_ENTERED,
    payload: board
})

export const passOutYearEntered = passOutYear => ({
    type: ACADEMIC_DETAIL_ACTION.PASS_OUT_YEAR_ENTERED,
    payload: passOutYear
})

export const percentageOfMarksEntered = percentageOfMarks => ({
    type: ACADEMIC_DETAIL_ACTION.PERCENTAGE_OF_MARKS_ENTERED,
    payload: percentageOfMarks
})

export const isHighestEducationEntered = isHighestEducation => ({
    type: ACADEMIC_DETAIL_ACTION.IS_HIGHEST_EDUCATION_ENTERED,
    payload: isHighestEducation
})


export const academicDetailSubmitted = (history) => (dispatch, getState) => {
    let personalDetailAPI = API + "/user/secure/academicInfo";
    fetch(personalDetailAPI, {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            id: getState().appState.studentID,
            ...getState().academicDetail
        })
    }).then(response => response.text().then(data => {
        dispatch({
            type: APP_STATE_ACTION.ACADEMIC_DATA_FILLED,
            payload: true
        });
        history.push("/edit/document");
    }))

}


export const Academic_Action = {
    nameEntered: nameEntered,
    boardEntered: boardEntered,
    passOutYearEntered: passOutYearEntered,
    percentageOfMarksEntered: percentageOfMarksEntered,
    isHighestEducationEntered: isHighestEducationEntered,
    academicDetailSubmitted: academicDetailSubmitted
}