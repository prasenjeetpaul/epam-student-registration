import { DOCUMENT_DETAIL_ACTION , APP_STATE_ACTION } from './actionType';
import { API } from "./../store/appState";

import axios from 'axios';
require ("babel-polyfill");

export const aadharUploaded = aadharData => ({
    type: DOCUMENT_DETAIL_ACTION.AADHAR_UPLOADED,
    payload: aadharData
})

export const resumeUploaded = resumeData => ({
    type: DOCUMENT_DETAIL_ACTION.RESUME_UPLOADED,
    payload: resumeData
})

export const documentUploaded = history => (dispatch, getState) => {
    let aadhar = getState().documentDetail.aadharData;
    let resume = getState().documentDetail.resumeData;
    let aadharAPI = API + "/user/secure?id=" 
        + getState().appState.studentID
        + "&type=image";
    let resumeAPI = API + "/user/secure?id=" 
        + getState().appState.studentID
        + "&type=resume";
    axios.post(aadharAPI, aadhar).then((response) => {
    });
    axios.post(resumeAPI, resume).then((response) => {
    });
    dispatch({
        type: APP_STATE_ACTION.DOCUMENT_UPLOADED,
        payload: true
    });
    history.push("/");
}

export const DocumentAction = {
    resumeUploaded: resumeUploaded,
    aadharUploaded: aadharUploaded,
    documentUploaded: documentUploaded
} 