const ErrorMessage = props => (
    <span className="error-txt">
        {props.message}
    </span>
);

export default ErrorMessage;