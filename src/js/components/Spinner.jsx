import "../../css/spinner.css";

const Spinner = props => (
    <div className={props.className}>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
    </div>
);

export default Spinner;