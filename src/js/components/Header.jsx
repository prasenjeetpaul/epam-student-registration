import "./../../css/header.css";

const Header = props => (
    <header>
        <div className="header-content">
            {
                props.noImage
                ?   null
                :  <img className="header-img" />
            }
            <div className="header-text">
                <h1>EPAM STUDENTS</h1>
            </div>
        </div>
    </header>
)

export default Header;