import "./../../css/footer.css";

const Footer = () => (
    <footer>
        <div className="footer-content">
            <span>&copy; 2018 EPAM SYSTEMS. &nbsp;ALL RIGHT RESERVED</span>
        </div>
    </footer>
)

export default Footer;