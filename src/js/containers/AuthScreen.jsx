import React from "react";
import { Route, Switch } from "react-router-dom";
import Login from "./Login";
import Register from "./Register";
import Header from "./../components/Header";
import Footer from "./../components/Footer";
import "./../../css/main.css";
import "./../../css/form.css";

class AuthScreen extends React.Component {
    render() {
        return (
            <section>
                <Header />
                <main>
                    <section className="main-auth-content">
                        <Switch>
                            <Route path="/login" component={Login} />
                            <Route path="/register" component={Register} />
                            <Route path="*" component={Login} />
                        </Switch>
                    </section>
                </main>
                <Footer />
            </section>
        )
    }
}

export default AuthScreen;