import React from "react";
import { Switch, Route} from "react-router-dom";
import Home from "./Home";
import AuthScreen from "./AuthScreen";
import "../../css/index.css";

class App extends React.Component {
    render() {
        return (
            <Switch>
                <Route exact path="/" component={Home} />
                <Route path="/edit" component={Home} />
                <Route path="*" component={AuthScreen} />
            </Switch>
        )
    }
}

export default App;