import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Redirect, Link } from "react-router-dom";
import { registerEmailEntered, registerPassword1Entered, registerPassword2Entered, registerClicked} from "./../actions/registerAction";
import InputTag from "./../components/InputTag";
import Button from "./../components/Button";
import ErrorMessage from "./../components/ErrorMessage";

class Register extends React.Component {
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(event) {
        event.preventDefault();
        this.props.registerClicked();
    }

    render() {
        if (this.props.isLoggedIn) {
            return <Redirect to="/"/>
        }
        return (
            <div className="auth-content">
                <form onSubmit={this.handleSubmit}>
                    <span className="auth-text">REGISTER</span>
                    <InputTag
                        type="text"
                        placeholder="Email"
                        value={this.props.emailID}
                        onChange={event => this.props.registerEmailEntered(event.target.value)} />
                    <InputTag
                        type="password"
                        placeholder="Password"
                        value={this.props.password1}
                        onChange={event => this.props.registerPassword1Entered(event.target.value)} />
                    <InputTag
                        type="password"
                        placeholder="Confirm Password"
                        value={this.props.password2}
                        onChange={event => this.props.registerPassword2Entered(event.target.value)} />
                    <Button
                        type="submit"
                        label="REGISTER" />
                </form>
                <div className="auth-helper">
                    {
                        this.props.registerError != null 
                        ? <ErrorMessage message={this.props.registerError}/>
                        : null
                    }
                    <span>
                        Already registered? <Link to="/login">Login now</Link>
                    </span>
                </div>
            </div>
        )
    }
}

const mapDispatchToProps = dispatch => (
    bindActionCreators({
        registerEmailEntered: registerEmailEntered,
        registerPassword1Entered: registerPassword1Entered,
        registerPassword2Entered: registerPassword2Entered,
        registerClicked: registerClicked,
    }, dispatch)
);

const mapStateToProps = state => ({
    emailID: state.register.emailId,
    password1: state.register.password1,
    password2: state.register.password2,
    isLoggedIn: state.appState.isLoggedIn,
    registerError: state.appState.registerError
});

export default connect(mapStateToProps, mapDispatchToProps)(Register);