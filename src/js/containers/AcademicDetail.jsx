import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { bindActionCreators } from "redux";
import { Academic_Action } from "./../actions/academicDetailAction";
import InputTag from "./../components/InputTag";
import Spinner from "./../components/Spinner";

class AcademicDetail extends React.Component {
    constructor(props) {
        super(props);
        this.nextClicked = this.nextClicked.bind(this);
        this.state = {
            spinnerClass: ["hide"]
        }
    }

    nextClicked() {
        this.props.academicDetailSubmitted(this.props.history);
        this.setState({
            spinnerClass: ["lds-roller"]
        })
    }

    render() {
        return (
            <section class="main-auth-content">
                <div class="auth-content">
                    <form>
                        <span class="auth-text">Enter your Academic Details</span>
                        <InputTag
                            type="text"
                            placeholder="Institute Name"
                            value={this.props.name}
                            onChange={(event) => this.props.nameEntered(event.target.value)}/>
                        <InputTag
                            type="text"
                            placeholder="Board Name"
                            value={this.props.board}
                            onChange={(event) => this.props.boardEntered(event.target.value)}/>
                        <InputTag
                            type="number"
                            placeholder="Passout Year"
                            value={this.props.passoutYear}
                            onChange={(event) => this.props.passOutYearEntered(event.target.value)}/>
                        <InputTag
                            type="number"
                            placeholder="Percentage"
                            value={this.props.percentageOfMarks}
                            onChange={(event) => this.props.percentageOfMarksEntered(event.target.value)}/>
                        <InputTag
                            type="text"
                            placeholder="Is Highest Education?"
                            value={this.props.isHighestEducation}
                            onChange={(event) => this.props.isHighestEducationEntered(event.target.value)}/>
                    </form>
                    <div>
                        <Link to="/edit/address"><button class="btn next-btn">PREV</button></Link>
                        <button class="btn next-btn" type="submit"  onClick={this.nextClicked}>NEXT</button>
                    </div>

                    <Spinner className={this.state.spinnerClass.join(" ")} />
                </div>
            </section>
        )
    }
}

const mapDispatchToProps = dispatch => (
    bindActionCreators({
        nameEntered: Academic_Action.nameEntered,
        boardEntered: Academic_Action.boardEntered,
        passOutYearEntered: Academic_Action.passOutYearEntered,
        percentageOfMarksEntered: Academic_Action.percentageOfMarksEntered,
        isHighestEducationEntered: Academic_Action.isHighestEducationEntered,
        academicDetailSubmitted: Academic_Action.academicDetailSubmitted,
    }, dispatch)
);

const mapStateToProps = state => ({
    name: state.academicDetail.name,
    board: state.academicDetail.board,
    passOutYear: state.academicDetail.passOutYear,
    percentageOfMarks: state.academicDetail.percentageOfMarks,
    isHighestEducation: state.academicDetail.isHighestEducation
});

export default connect(mapStateToProps, mapDispatchToProps)(AcademicDetail);