import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { firstNameEntered, 
            middleNameEntered, 
            lastNameEntered, 
            dateOfBirthEntered, 
            mobileNumberEntered, 
            personalDetailSubmitted } from "./../actions/personalDetailAction";
import InputTag from "./../components/InputTag";
import Spinner from "./../components/Spinner";

class PersonalDetail extends React.Component {
    constructor(props) {
        super(props);
        this.nextClicked = this.nextClicked.bind(this);
        this.state = {
            spinnerClass: ["hide"]
        }
    }

    nextClicked() {
        this.props.personalDetailSubmitted(this.props.history);
        this.setState({
            spinnerClass: ["lds-roller"]
        })
    }

    render() {
        return (
            <section class="main-auth-content">
                <div class="auth-content">
                    <form onSubmit={this.nextClicked}>
                        <span class="auth-text">Enter your Personal Details</span>
                        <InputTag
                            type="text"
                            placeholder="First Name"
                            value={this.props.firstName}
                            onChange={(event) => this.props.firstNameEntered(event.target.value)}/>
                        <InputTag
                            type="text"
                            placeholder="Middle Name"
                            value={this.props.middleName}
                            onChange={(event) => this.props.middleNameEntered(event.target.value)}/>
                        <InputTag
                            type="text"
                            placeholder="Last Name"
                            value={this.props.lastName}
                            onChange={(event) => this.props.lastNameEntered(event.target.value)}/>
                        <InputTag
                            type="date"
                            placeholder="Date of Birth (DD-MM-YYYY)"
                            value={this.props.dateOfBirth}
                            onChange={(event) => this.props.dateOfBirthEntered(event.target.value)}/>
                        <InputTag
                            type="number"
                            placeholder="Mobile Number"
                            value={this.props.mobileNumber}
                            onChange={(event) => this.props.mobileNumberEntered(event.target.value)}/>
                    </form>
                    <div>
                        {/* <button class="btn next-btn" type="submit"> PREV</button> */}
                        <button class="btn next-btn" type="submit" onClick={this.nextClicked}>NEXT</button>
                    </div>

                    <Spinner className={this.state.spinnerClass.join(" ")} />
                </div>
            </section>
        )
    }
}

const mapDispatchToProps = dispatch => (
    bindActionCreators({
        firstNameEntered: firstNameEntered,
        middleNameEntered: middleNameEntered,
        lastNameEntered: lastNameEntered,
        dateOfBirthEntered: dateOfBirthEntered,
        mobileNumberEntered: mobileNumberEntered,
        personalDetailSubmitted: personalDetailSubmitted,
    }, dispatch)
);

const mapStateToProps = state => ({
    firstName: state.personalDetail.firstName,
    middleName: state.personalDetail.middleName,
    lastName: state.personalDetail.lastName,
    dateOfBirth: state.personalDetail.dateOfBirth,
    mobileNumber: state.personalDetail.mobileNumber
});

export default connect(mapStateToProps, mapDispatchToProps)(PersonalDetail);