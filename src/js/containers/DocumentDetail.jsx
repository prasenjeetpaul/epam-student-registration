import React from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { DocumentAction } from "../actions/documentDetailAction";

class DocumentDetail extends React.Component {
    constructor(props) {
        super(props);
        this.uploadAadhar = this.uploadAadhar.bind(this);
        this.uploadResume = this.uploadResume.bind(this);
    }

    uploadAadhar(event) {
        let file = event.target.files[0];
        if (file) {
            let data = new FormData();
            data.append('file', file);
            this.props.aadharUploaded(data);
        }
    }

    uploadResume(event) {
        let file = event.target.files[0];
        if (file) {
            let data = new FormData();
            data.append('file', file);
            this.props.resumeUploaded(data);
        }
    }


    render() {
        return (
            <section class="main-auth-content">
                <div class="auth-content">
                    <form>
                        <span class="auth-text">Upload your Documents</span>
                        <div class="auth-file">
                            <span>Aadhar Card: </span>
                            <input 
                                class="auth-input" 
                                name="aadhar" 
                                type="file" 
                                accept="image/*" 
                                onChange={this.uploadAadhar}/>
                        </div>
                        <div class="auth-file">
                            <span>Resume: </span>
                            <input 
                                class="auth-input" 
                                name="resume" 
                                type="file" 
                                onChange={this.uploadResume}/>
                        </div>
                    </form>
                    <div>
                        <Link to="/edit/academic"><button class="btn next-btn">PREV</button></Link>
                        <button class="btn next-btn" type="submit" onClick={() => this.props.documentUploaded(this.props.history)}>NEXT</button>
                    </div>
                </div>
            </section>

        )
    }
}

const mapDispatchToProps = dispatch => (
    bindActionCreators({
        aadharUploaded: DocumentAction.aadharUploaded,
        resumeUploaded: DocumentAction.resumeUploaded,
        documentUploaded: DocumentAction.documentUploaded,
    }, dispatch)
)

export default connect(null, mapDispatchToProps)(DocumentDetail);