import React from "react";
import { connect } from "react-redux";
import { Redirect, Switch, Link, Route} from "react-router-dom";
import { bindActionCreators } from "redux";
import { logoutClicked } from "./../actions/loginAction";
import ApplicationStatus from "./../components/ApplicationStatus";
import PersonalDetail from "./PersonalDetail";
import AddressDetail from "./AddressDetail";
import AcademicDetail from "./AcademicDetail";
import DocumentDetail from "./DocumentDetail";
import Header from "./../components/Header";
import Footer from "../components/Footer";
import "./../../css/main.css";
import "./../../css/home.css";
import "./../../css/form.css";

class Home extends React.Component {

    render() {
        if (!this.props.isLoggedIn) {
            return <Redirect to="/login" />
        }
        return (
            <section>
                <div class="sidenav">
                    <img class="header-img" />
                    <Link to="/">Application Status</Link>
                    <Link to="/edit/personal">Perosonal Details
                        { this.props.isPersonalFilled
                            ? " ✔"
                            : null
                        }
                    </Link>
                    <Link to="/edit/address">Address Details
                        { this.props.isAddressFilled
                            ? " ✔"
                            : null
                        }
                    </Link>
                    <Link to="/edit/academic">Academic Details
                        { this.props.isAcademicFilled
                            ? " ✔"
                            : null
                        }
                    </Link>
                    <Link to="/edit/document">Documents
                        { this.props.isDocumentUploaded
                            ? " ✔"
                            : null
                        }
                    </Link>
                    <a onClick={this.props.logoutClicked}>Logout &#9658;</a>
                </div>

                <div class="main">
                    <Header noImage={true}/>
                    <main class="home-main">
                        <Switch>
                            <Route exact path="/" component={ApplicationStatus} />
                            <Route path="/edit/personal" component={PersonalDetail} />
                            <Route path="/edit/address" component={AddressDetail} />
                            <Route path="/edit/academic" component={AcademicDetail} />
                            <Route path="/edit/document" component={DocumentDetail} />
                        </Switch>
                    </main>
                    <Footer />
                </div>
            </section>
        )
    }
}

const mapStateToProps = state => ({
    isLoggedIn: state.appState.isLoggedIn,
    isPersonalFilled: state.appState.isPersonalFilled,
    isAddressFilled: state.appState.isAddressFilled,
    isAcademicFilled: state.appState.isAcademicFilled,
    isDocumentUploaded: state.appState.isDocumentUploaded,
})

const mapDispatchToProps = dispatch => (
    bindActionCreators({
        logoutClicked: logoutClicked
    }, dispatch)
)
export default connect(mapStateToProps, mapDispatchToProps)(Home);