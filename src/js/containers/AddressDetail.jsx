import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { bindActionCreators } from "redux";
import { AddressAction } from "./../actions/addressDetailAction";
import InputTag from "./../components/InputTag";
import Spinner from "./../components/Spinner";
import Dropdown from "./Dropdown";

class AddressDetail extends React.Component {
    constructor(props) {
        super(props);
        this.nextClicked = this.nextClicked.bind(this);
        this.state = {
            spinnerClass: ["hide"],
        }
        this.props.fetchCountry();
    }

    nextClicked() {
        this.props.addressDetailSubmitted(this.props.history);
        this.setState({
            spinnerClass: ["lds-roller"]
        })
    }

    render() {
        return (
            <section class="main-auth-content">
                <div class="auth-content">
                    <form>
                        <span class="auth-text">Enter your Address Details</span>
                        <InputTag
                            type="text"
                            placeholder="Type"
                            value={this.props.firstName}
                            onChange={(event) => this.props.typeEntered(event.target.value)}/>
                        <InputTag
                            type="text"
                            placeholder="House Number"
                            value={this.props.middleName}
                            onChange={(event) => this.props.houseNumberEntered(event.target.value)}/>
                        <InputTag
                            type="text"
                            placeholder="Street"
                            value={this.props.lastName}
                            onChange={(event) => this.props.streetEntered(event.target.value)}/>
                        <InputTag
                            type="text"
                            placeholder="Landmark"
                            value={this.props.lastName}
                            onChange={(event) => this.props.landmarkEntered(event.target.value)}/>
                        <InputTag
                            type="text"
                            placeholder="City"
                            value={this.props.dateOfBirth}
                            onChange={(event) => this.props.cityEntered(event.target.value)}/>
                        <InputTag
                            type="number"
                            placeholder="Zipcode"
                            value={this.props.lastName}
                            onChange={(event) => this.props.zipcodeEntered(event.target.value)}/>
                        {
                            this.props.country != null 
                            ?   <Dropdown 
                                    data={this.props.country}
                                    type="COUNTRY"
                                />
                            :   null
                        }

                        {
                            this.props.state != null && this.props.state.length > 0
                            ?   <Dropdown 
                                    data={this.props.state}
                                    type="STATE"
                                />
                            :   null
                        }
                        
                        
                    </form>
                    <div>
                        <Link to="/edit/personal"><button class="btn next-btn">PREV</button></Link>
                        <button class="btn next-btn" type="submit"  onClick={this.nextClicked}>NEXT</button>
                    </div>

                    <Spinner className={this.state.spinnerClass.join(" ")} />
                </div>
            </section>

        )
    }
}

const mapDispatchToProps = dispatch => (
    bindActionCreators({
        typeEntered: AddressAction.typeEntered,
        houseNumberEntered: AddressAction.houseNumberEntered,
        streetEntered: AddressAction.streetEntered,
        landmarkEntered: AddressAction.landmarkEntered,
        cityEntered: AddressAction.cityEntered,
        zipcodeEntered: AddressAction.zipcodeEntered,
        addressDetailSubmitted: AddressAction.addressDetailSubmitted,

        fetchCountry: AddressAction.fetchCountry
    }, dispatch)
);

const mapStateToProps = state => ({
    type: state.addressDetail.type,
    houseNumber: state.addressDetail.houseNumber,
    street: state.addressDetail.street,
    landmark: state.addressDetail.landmark,
    city: state.addressDetail.city,
    state: state.addressDetail.state,
    zipcode: state.addressDetail.zipcode,
    country: state.addressDetail.country,
});

export default connect(mapStateToProps, mapDispatchToProps)(AddressDetail);