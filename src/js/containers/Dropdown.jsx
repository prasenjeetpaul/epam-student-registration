import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import DropdownItem from "../components/DropdownItem";
import { fetchState, selectedStateEntered } from "./../actions/addressDetailAction";

class Dropdown extends React.Component {
    constructor(props) {
        super(props);
        this.optionSelected = this.optionSelected.bind(this);
    }

    optionSelected(event) {
        let selectedValue = event.target.value;
        if (this.props.type == "COUNTRY") {
            this.props.fetchState(selectedValue)
        } else if (this.props.type == "STATE"){
            this.props.selectedStateEntered(selectedValue)
        }
    }

    render() {
        return (
            <select className="auth-input" onChange={this.optionSelected}>
                {
                    this.props.type == "COUNTRY"
                    ?   <option value="" selected disabled>Select Country</option>
                    :   this.props.type == "STATE"
                        ?   <option value="" selected disabled>Select State</option>
                        :   null
                }
                {this.props.data.map(individualData => (
                    this.props.type == "COUNTRY"
                    ?   <DropdownItem name={individualData}/>
                    :   this.props.type == "STATE" 
                        ?   <DropdownItem name={individualData}/>
                        : null
                ))}
            </select>
        )
    }
}

const mapDispatchToProps = dispatch => (
    bindActionCreators({
        fetchState: fetchState,
        selectedStateEntered: selectedStateEntered,
    }, dispatch)
);

export default connect(null, mapDispatchToProps)(Dropdown);