import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Redirect, Link} from "react-router-dom";
import { loginEmailEntered, loginPasswordEntered, loginClicked} from "./../actions/loginAction";
import InputTag from "./../components/InputTag";
import Button from "./../components/Button";
import ErrorMessage from  "./../components/ErrorMessage";

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(event) {
        event.preventDefault();
        this.props.loginClicked();
    }

    render() {
        if (this.props.isLoggedIn) {
            return <Redirect to="/"/>
        }
        return (
            <div className="auth-content ">
                <form onSubmit={this.handleSubmit}>
                    <span className="auth-text">LOGIN</span>
                    <InputTag 
                        type="text"
                        placeholder="Email"
                        value={this.props.emailID}
                        onChange={event => this.props.loginEmailEntered(event.target.value)} />
                    <InputTag
                        type="password"
                        placeholder="Password"
                        value={this.props.password}
                        onChange={event => this.props.loginPasswordEntered(event.target.value)} />
                    <Button
                        type="submit"
                        label="LOGIN" />
                </form>
                <div className="auth-helper">
                    {
                        this.props.loginError != null 
                        ? <ErrorMessage message={this.props.loginError}/>
                        : null
                    }
                    <span>
                        Not yet registered? <Link to="/register">Register for an account</Link>
                    </span>
                </div>
            </div>
        )
    }
}

const mapDispatchToProps = dispatch => (
    bindActionCreators({
        loginEmailEntered: loginEmailEntered,
        loginPasswordEntered: loginPasswordEntered,
        loginClicked: loginClicked
    }, dispatch)
);

const mapStateToProps = state => ({
    emailID: state.login.emailId,
    password: state.login.password,
    isLoggedIn: state.appState.isLoggedIn,
    loginError: state.appState.loginError
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);