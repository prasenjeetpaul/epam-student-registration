const appState = {
    studentID: null,
    
    isLoggedIn: false,
    loginError: null,
    registerError: null,

    isPersonalFilled: false,
    isAddressFilled: false,
    isAcademicFilled: false,
    isDocumentUploaded: false,
}

export default appState;

export const API = "http://localhost:8080";