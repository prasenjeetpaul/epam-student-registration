const addressDetailState = {
    type: "",
    houseNumber: "",
    street: "",
    landmark: "",
    city: "",
    state: null,
    zipCode: "",
    country: null,

    selectedCountry: "",
    selectedState: "",
}

export default addressDetailState;