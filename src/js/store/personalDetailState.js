const personalDetailState = {
    firstName: "",
    middleName: "",
    lastName: "",
    dateOfBirth: "",
    mobileNumber: ""
}

export default personalDetailState;