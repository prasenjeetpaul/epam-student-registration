import { combineReducers } from 'redux';
import loginReducer from './loginReducer';
import registerReducer from "./registerReducer";
import appStateReducer from "./appStateReducer";
import personalDetailReducer from "./personalDetailReducer";
import academicDetailReducer from "./academicDetailReducer";
import addressDetailReducer from "./addressDetailReducer";
import documentDetailReducer from "./documentDetailReducer";

export default combineReducers({
    appState: appStateReducer,
    login: loginReducer,
    register: registerReducer,
    personalDetail: personalDetailReducer,
    addressDetail: addressDetailReducer,
    academicDetail: academicDetailReducer,
    documentDetail: documentDetailReducer,
});
