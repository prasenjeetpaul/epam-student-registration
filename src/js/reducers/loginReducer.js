import { LOGIN_ACTION } from "./../actions/actionType";
import loginInitialState from "./../store/loginState";

export default function(state = loginInitialState, action) {
    switch(action.type) {
        case LOGIN_ACTION.EMAIL_ENTERED: {
            return {
                ...state,
                emailId: action.payload
            }
        }

        case LOGIN_ACTION.PASSWORD_ENTERED: {
            return {
                ...state,
                password: action.payload
            }
        }

        default: {
            return state;
        }
    }
}