import { DOCUMENT_DETAIL_ACTION } from "../actions/actionType";
import documentDetailState from "../store/documentDetailState";

export default function(state = documentDetailState, action) {
    switch(action.type) {
        case DOCUMENT_DETAIL_ACTION.AADHAR_UPLOADED: {
            return {
                ...state,
                aadharData: action.payload
            }
        }
        
        case DOCUMENT_DETAIL_ACTION.RESUME_UPLOADED: {
            return {
                ...state,
                resumeData: action.payload
            }
        }

        default: {
            return state;
        }
    }
}